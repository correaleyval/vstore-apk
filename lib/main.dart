import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './views/MainView.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'VStore',
      home: MainView(),
    );
  }
}