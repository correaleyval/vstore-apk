import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:http/http.dart' as http;

class Product {
  Product({
    @required this.name,
    @required this.description,
    this.imageFile,
    this.imageUrl
  });

  String name;
  String description;
  File imageFile;
  String imageUrl;
}

class ProductList {
  List _products = [];

  getProducts() async {
    Map data;

    http.Response response = await http.get('http://10.42.0.128:3000/products');

    data = json.decode(response.body);

    _products = data['products'];

    return _products;
  }

  bool sending;

  void add(Product p) async {
    await http.post('http://10.42.0.128:3000/products', body: {
      'name': p.name,
      'description': p.description,
      'image': base64Encode(p.imageFile.readAsBytesSync()),
      'format': p.imageFile.path.split('/').last.split('.').last
    })
    .catchError((err) {
      debugPrint(err);
    });
  }

  operator [](int i) => _products.elementAt(i);

  get length => _products.length;
}

ProductList productList = ProductList();
