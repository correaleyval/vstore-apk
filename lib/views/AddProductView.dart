import 'dart:io';
import 'package:flutter/material.dart';
import '../models/ProductModel.dart';
import 'package:image_picker_modern/image_picker_modern.dart';

import './AddProductViewInfo.dart';

class AddProductView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddProductViewState();
  }
}

class AddProductViewState extends State<AddProductView> {
  String _name;
  String _description;

  File _imageFile;

  var a = Directory('.');

  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Widget> viewList;

  @override
  Widget build(BuildContext context) {
    viewList = getViewList();

    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: FloatingActionButton(
        tooltip: 'Crear producto',
        child: const Icon(Icons.check),
        backgroundColor: Theme.of(context).accentColor,
        onPressed: () async {
          if (_imageFile != null) {
            var data = await Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => AddProductViewInfo(
                      imageFile: _imageFile,
                    ))) as Map<String, String>;

            _name = data['name'];
            _description = data['description'];
          }
          
          _saveProduct();
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: AppBar(
        title: Text("Crear anuncio", style: TextStyle(color: Colors.blue)),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.blue),
        centerTitle: true,
      ),
      body: viewList[_barIndex],
      bottomNavigationBar: _builtBottomBar(),
    );
  }

  int _barIndex = 0;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _imageFile = image;
    });
  }

  Future getImageCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _imageFile = image;
    });
  }

  BottomAppBar _builtBottomBar() {
    return BottomAppBar(
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.photo), title: Text(" ")),
            BottomNavigationBarItem(
                icon: Icon(Icons.photo_camera), title: Text(" ")),
          ],
          unselectedItemColor: Colors.blue,
          selectedItemColor: Colors.blue,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          iconSize: 30,
          currentIndex: _barIndex,
          onTap: _onTapbottomBar,
        ),
        shape: CircularNotchedRectangle(),
        clipBehavior: Clip.antiAlias,
        notchMargin: 6);
  }

  void _onTapbottomBar(int ele) {
    setState(() {
      _imageFile = null;
      _barIndex = ele;

      if (ele == 0)
        getImage();
      else
        getImageCamera();
    });
  }

  void _saveProduct() {
    if (_name != null && _description != null && _imageFile != null) {
      productList.add(Product(
          name: _name, description: _description, imageFile: _imageFile));

      Navigator.of(context).pop();
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
              'Debe completar todos los datos solicitados para poder crear un anuncio')));
    }
  }

  getViewList() {
    return [
      Center(
          child: _imageFile == null
              ? FlatButton(
                  child: Icon(Icons.photo_album, size: 128, color: Colors.blue),
                  onPressed: getImage,
                )
              : FlatButton(
                  child: Center(
                    child: Image.file(_imageFile, width: 350, height: 350),
                  ),
                  onPressed: getImage,
                )),
      Center(
          child: _imageFile == null
              ? FlatButton(
                  child: Icon(Icons.add_a_photo, size: 128, color: Colors.blue),
                  onPressed: getImageCamera,
                )
              : FlatButton(
                  child: Center(
                    child: Image.file(_imageFile, width: 350, height: 350),
                  ),
                  onPressed: getImage,
                )),
    ];
  }
}
