import 'package:flutter/material.dart';
import 'dart:io';

class AddProductViewInfo extends StatefulWidget {
  final File imageFile;

  AddProductViewInfo({@required this.imageFile}) : assert(imageFile != null);

  @override
  State<StatefulWidget> createState() {
    return AddProductViewInfoState();
  }
}

class AddProductViewInfoState extends State<AddProductViewInfo> {
  String name;
  String description;

  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title:
            Text("Describa su producto", style: TextStyle(color: Colors.blue)),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.blue),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Crear producto',
        child: const Icon(Icons.check),
        backgroundColor: Theme.of(context).accentColor,
        onPressed: () {
          if (name != null && description != null)
            Navigator.pop(context, {
              'name': name,
              'description': description
            });
          else
            _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text(
                    'Debe completar todos los datos solicitados para poder crear un anuncio')));
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      bottomNavigationBar: _builtBottomBar(),
      body: SingleChildScrollView(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.file(widget.imageFile, width: 64, height: 64),
              Flexible(
                child: Padding(
                    padding: EdgeInsets.all(4),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Nombre del Producto',
                      ),
                      onChanged: (value) {
                        name = value;
                      },
                    )),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.all(6),
            child: TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Agregue una breve descripción sobre su producto',
                labelText: 'Descripción',
              ),
              maxLines: 4,
              onChanged: (value) {
                description = value;
              },
            ),
          ),
        ],
      )),
    );
  }

  BottomAppBar _builtBottomBar() {
    return BottomAppBar(
        child: ButtonBar(
          children: <Widget>[],
        ),
        shape: CircularNotchedRectangle(),
        clipBehavior: Clip.antiAlias,
        notchMargin: 6);
  }
}