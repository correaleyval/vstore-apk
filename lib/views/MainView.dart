import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './ProductView.dart';
import './SearchView.dart';
import './AddProductView.dart';
import './NotificationView.dart';
import './MessageView.dart';

class MainView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainViewState();
  }
}

class MainViewState extends State<MainView> {
  int _barIndex = 0;

  @override
  Widget build(BuildContext context) {
    List<Widget> viewList = [
      ProductView(),
      SearchView(),
      null,
      NotificationView(),
      MessageView(),
    ];

    return Scaffold(
      body: viewList[_barIndex],
      bottomNavigationBar: _buildBottomBar(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              fullscreenDialog: true,
              builder: (BuildContext context) => AddProductView()));
        },
        child: const Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  BottomAppBar _buildBottomBar() {
    return BottomAppBar(
      child: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: _onTapbottomBar, 
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text(" ")),
          BottomNavigationBarItem(icon: Icon(Icons.search), title: Text(" ")),
          BottomNavigationBarItem(icon: Icon(null), title: Text(" ")),
          BottomNavigationBarItem(icon: Icon(Icons.notifications), title: Text(" ")),
          BottomNavigationBarItem(icon: Icon(Icons.message), title: Text(" ")),
        ],
        unselectedItemColor: Colors.blue,
        selectedItemColor: Colors.blue,
        selectedFontSize: 0,
        unselectedFontSize: 0,
        iconSize: 30,
        currentIndex: _barIndex,
      ),
      shape: CircularNotchedRectangle(),
      clipBehavior: Clip.antiAlias,
      notchMargin: 6
    );
  } // _bottomBar

  void _onTapbottomBar(int ele) {
    setState(() {
      if(ele != 2)
        _barIndex = ele;
    });
  }
}
