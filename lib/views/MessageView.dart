import 'package:flutter/material.dart';

class MessageView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MessageViewState();
  }
}

class MessageViewState extends State<MessageView> {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text("Message View"));
  }
}